# Test

這是來自歐尼克斯實境互動工作室(OmniXRI Studio)的一個測試專案，更多資訊可參考 https://omnixri.blogspot.com/

歐尼克斯實境互動工作室(Omni eXtended Reality Interaction Studio, OmniXRI)主要興趣為虛實顯示及互動，歡迎大家一起交流。

本工作室除部落格、FB社團提供免費資訊外，另提供專業商業服務，包括系統整合開發、技術顧問諮詢、新創企業輔導、教育訓練規畫，領域包含機電整合、電腦視覺、立體顯示、實境互動、人工智慧、智財技轉等，如有需求歡迎洽詢。

Facebook : Jack Omnixri https://www.facebook.com/jack.omnixri.5

FB社團 : [Edge AI Taiwan] https://www.facebook.com/groups/2603355889951761/

電子信箱：omnixri@gmail.com

部落格：https://omnixri.blogspot.tw

Medium：https://medium.com/@omnixri

Github開源：https://github.com/OmniXRI